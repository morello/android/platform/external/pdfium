// Copyright (c) 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef THIRD_PARTY_BASE_ALLOCATOR_PARTITION_ALLOCATOR_PARTITION_FREELIST_ENTRY_H_
#define THIRD_PARTY_BASE_ALLOCATOR_PARTITION_ALLOCATOR_PARTITION_FREELIST_ENTRY_H_

#ifdef __CHERI__
#include <cheriintrin.h>
#endif
#include <stdint.h>

#include "build/build_config.h"
#include "third_party/base/allocator/partition_allocator/partition_alloc_constants.h"
#include "third_party/base/compiler_specific.h"
#include "third_party/base/sys_byteorder.h"

namespace pdfium {
namespace base {
namespace internal {

struct EncodedPartitionFreelistEntry;

struct PartitionFreelistEntry {
  EncodedPartitionFreelistEntry* next;

  PartitionFreelistEntry() = delete;
  ~PartitionFreelistEntry() = delete;

  ALWAYS_INLINE static EncodedPartitionFreelistEntry* Encode(
      PartitionFreelistEntry* ptr) {
    return reinterpret_cast<EncodedPartitionFreelistEntry*>(Transform(ptr, true));
  }

#ifdef __CHERI_PURE_CAPABILITY__
  static ALWAYS_INLINE void InitSealingKey(uintcap_t key) {
    sealing_key_capability = key;
  }
#endif

 private:
  friend struct EncodedPartitionFreelistEntry;

#ifdef __CHERI_PURE_CAPABILITY__
  static uintcap_t sealing_key_capability;
#endif

  static ALWAYS_INLINE void* Transform(void* ptr, bool encode) {
#ifdef __CHERI_PURE_CAPABILITY__
    if (ptr != nullptr) {
      if (encode) {
        return cheri_seal(ptr, sealing_key_capability);
      } else {
        return cheri_unseal(ptr, sealing_key_capability);
      }
    } else {
      return nullptr;
    }
#else /* !__CHERI_PURE_CAPABILITY__ */
    (void)encode;
    // We use bswap on little endian as a fast mask for two reasons:
    // 1) If an object is freed and its vtable used where the attacker doesn't
    // get the chance to run allocations between the free and use, the vtable
    // dereference is likely to fault.
    // 2) If the attacker has a linear buffer overflow and elects to try and
    // corrupt a freelist pointer, partial pointer overwrite attacks are
    // thwarted.
    // For big endian, similar guarantees are arrived at with a negation.
#if defined(ARCH_CPU_BIG_ENDIAN)
    uintptr_t masked = ~reinterpret_cast<uintptr_t>(ptr);
#else
    uintptr_t masked = ByteSwapUintPtrT(reinterpret_cast<uintptr_t>(ptr));
#endif
    return reinterpret_cast<void*>(masked);
#endif /* !__CHERI_PURE_CAPABILITY__ */
  }
};

struct EncodedPartitionFreelistEntry {
  char scrambled[sizeof(PartitionFreelistEntry*)];

  EncodedPartitionFreelistEntry() = delete;
  ~EncodedPartitionFreelistEntry() = delete;

  ALWAYS_INLINE static PartitionFreelistEntry* Decode(
      EncodedPartitionFreelistEntry* ptr) {
    return reinterpret_cast<PartitionFreelistEntry*>(
        PartitionFreelistEntry::Transform(ptr, false));
  }
};

static_assert(sizeof(PartitionFreelistEntry) ==
                  sizeof(EncodedPartitionFreelistEntry),
              "Should not have padding");

}  // namespace internal
}  // namespace base
}  // namespace pdfium

#endif  // THIRD_PARTY_BASE_ALLOCATOR_PARTITION_ALLOCATOR_PARTITION_FREELIST_ENTRY_H_
