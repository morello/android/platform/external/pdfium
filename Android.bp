cc_defaults {
    name: "pdfium-common",
    cflags: [
        "-O3",
        "-fstrict-aliasing",
        "-fprefetch-loop-arrays",
        "-fexceptions",

        "-Wno-implicit-fallthrough",
        "-Wno-missing-field-initializers",
        "-Wno-non-virtual-dtor",
        "-Wno-unused-parameter",

        // pdfium_common_config
        "-DOPJ_STATIC",
        "-DPNG_PREFIX",
        "-DPNG_USE_READ_MACROS",
    ],

    arch: {
        arm: {
            instruction_set: "arm",
        },
    },

    include_dirs: [
        "external/freetype/include",
    ],

    compile_multilib: "both",

    header_libs: [
        "pdfium-headers",
        "pdfium-third-party-headers"
    ],
}

cc_defaults {
    name: "pdfium-core",

    defaults: [
        "pdfium-common"
    ],

    exclude_srcs: [
        "**/*_unittest.cpp",
        "**/*_embeddertest.cpp",
    ],
}


cc_library_headers {
    name: "pdfium-headers",
    export_include_dirs: ["."],
}

cc_library_headers {
    name: "pdfium-third-party-headers",
    export_include_dirs: ["third_party"],
}

cc_library {
    name: "libpdfium",
    defaults: ["pdfium-core"],

    header_libs: [
        "libpdfium-constants",
    ],

    whole_static_libs: [
        "libpdfium-fpdfsdk",
    ],

    // Transitivity is not supported for static libraries (yet).
    // Lists the whole transitivity closure here.
    static_libs: [
        "libpdfium-agg",
        "libpdfium-cmaps",
        "libpdfium-edit",
        "libpdfium-fdrm",
        "libpdfium-font",
        "libpdfium-formfiller",
        "libpdfium-fpdfdoc",
        "libpdfium-fpdftext",
        "libpdfium-fxcodec",
        "libpdfium-fxcrt",
        "libpdfium-fxge",
        "libpdfium-fxjs",
        "libpdfium-libopenjpeg2",
        "libpdfium-page",
        "libpdfium-parser",
        "libpdfium-pwl",
        "libpdfium-render",
        "libpdfium-skia_shared",
        "libpdfium-third_party-base",
        "libpdfium-lcms2",
    ],

    // TODO: figure out why turning on exceptions requires manually linking libdl
    shared_libs: [
        "libandroidicu",
        "libdl",
        "libft2",
        "libjpeg",
        "libz",
    ],

    export_include_dirs: ["public"],

}

cc_binary {
    name: "pdfium_test",
    defaults: ["pdfium-common"],

    srcs: [
        "samples/pdfium_test.cc",
        "samples/pdfium_test_dump_helper.cc",
        "samples/pdfium_test_event_helper.cc",
        "samples/pdfium_test_write_helper.cc",
        "testing/image_diff/image_diff_png.cpp",
        "testing/test_support.cpp",
        "testing/fx_string_testhelpers.cpp",
        "testing/invalid_seekable_read_stream.cpp",
        "testing/string_write_stream.cpp",
        "testing/test_loader.cpp",
        "testing/utils/bitmap_saver.cpp",
        "testing/utils/file_util.cpp",
        "testing/utils/hash.cpp",
        "testing/utils/path_service.cpp",
    ],

    header_libs: [
        "libpdfium-constants",
    ],

    whole_static_libs: [
        "libpdfium",
    ],

    static_libs: [
        "libpdfium-agg",
        "libpdfium-cmaps",
        "libpdfium-edit",
        "libpdfium-fdrm",
        "libpdfium-font",
        "libpdfium-formfiller",
        "libpdfium-fpdfdoc",
        "libpdfium-fpdftext",
        "libpdfium-fxcodec",
        "libpdfium-fxcrt",
        "libpdfium-fxge",
        "libpdfium-fxjs",
        "libpdfium-libopenjpeg2",
        "libpdfium-page",
        "libpdfium-parser",
        "libpdfium-pwl",
        "libpdfium-render",
        "libpdfium-skia_shared",
        "libpdfium-third_party-base",
        "libpdfium-lcms2",
    ],

    shared_libs: [
        "libandroidicu",
        "libdl",
        "libft2",
        "libjpeg",
        "libpng",
        "libz",
    ],

    multilib: {
        libc64: {
            stem: "pdfium_test-c64",
        },
    },
}

subdirs = ["third_party"]
